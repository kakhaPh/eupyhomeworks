"""
1)გამოთვალეთ შემდეგი გამოსახულება 4-2^3+5^2-3/2 და დაბეჭდეთ შედეგი ეკრანზე

"""


def task1():
    task = 4 - 2 ** 3 + 5 ** 2 - 3 / 2
    print(task)


# task1()


"""
2) დაწერეთ პროგრამა, რომელიც შეყვანილ ათობით რიცხვს გადაიყვანს ორობითში.

"""


def task2():
    decimal_num = input("Enter a decimal number: ")
    print(f"a={decimal_num}")
    print(bin(int(decimal_num)))


# task2()


"""
3) დაწერეთ პროგრამა, რომელიც user-ს შეეკითხება თვეში სამუშაო საათების რაოდენობას და საათობრივ სახელფასო განაკვეთს.
   შედეგად დაბეჭდავს ყოველთვიური ხელფასის ოდენობას.

"""


def task3():
    hoursPerMonth = input("თვეში სამუშაო საათების რაოდენობა: ")
    SalaryPerHour = input("სახელფასო განაკვეთი საათობრივად: ")
    SalaryPerMonth = int(hoursPerMonth) * int(SalaryPerHour)

    print(SalaryPerMonth)


# task3()


"""
4) დაწერეთ პროგრამა, სადაც მომხმარებელი შეიყვანს 3 რიცხვს და პროგრამა დაბეჭდავს რიცხვების საშუალო არითმეტიკულს.

"""


def task4():
    a = float(input("a = "))
    b = float(input("b = "))
    c = float(input("c = "))
    print((a + b + c) / 3)


# task4()


"""
5) დაწერეთ პროგრამა რომელიც შეეკითხება user-ს სახელს და ასაკს. გამოთვალეთ რამდენ წელში გახდება 100 წლის
   და დაბეჭდეთ შესაბამისი ინფორმაცია

"""


def task5():
    userName = input("თქვენი სახელი: ")
    userAge = input("თქვენი ასაკი: ")
    targetAge = 100 - int(userAge)

    print(userName, "თქვენ 100 წლის გახდებით", targetAge, "წელში")


# task5()


"""
6) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ნებისმიერ რიცხვს. თუ რიცხვი დადებითია, დაბეჭდოს
   ეკრანზე “Number is positive”.

"""


def task6():
    Num = int(input("შეიყვანეთ ნებისმიერი რიცხვი: "))
    if Num >= 0:
        print("Number is Positive")
    else:
        print("Number is negative")


# task6()


"""
7) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ნებისმიერ რიცხვს. პროგრამამ შეამოწმოს, თუ შეყვანილი რიცხვი
10-ის ჯერადია, დაბეჭდოს “რიცხვი ბოლოვდება 0-ით”, თუ არადა დაბეჭდოს “რიცხვი არ ბოლოვდება 0-ით”.
(გაითვალისწინეთ: 10-ის ჯერადი ნიშნავს რომ 10-ზე გაყოფისას ნაშთი არის 0).

"""


def task7():
    Num = int(input("ნებისმიერი რიცხვი:"))
    if Num % 10 == 0:
        print("რიცხვი ბოლოვდება 0-ით")
    else:
        print("რიცხვი არ ბოლოვდება 0-ით")


# task7()


"""
8) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ორ ნებისმიერ რიცხვს. პროგრამამ შეამოწმოს, თუ ორივე შეყვანილი
რიცხვი 10-ზე მეტია, დაითვალეთ მათი საშუალო არითმეტიკული, თუ არადა დაითვალეთ მათი ნამრავლი. დაბეჭდეთ მიღებული შედეგი.

"""


def task8():
    Num1 = int(input("პირველი რიცხვი: "))
    Num2 = int(input("მეორე რიცხვი: "))

    if Num1 > 10 and Num2 > 10:
        average = (Num1 + Num2) / 2
        print(average)
    else:
        print(Num1 * Num2)


# task8()


"""
9) დაწერეთ პროგრამა, რომლის გაშვებისას შეიყვანთ ნებისმიერ რიცხვს. იპოვეთ შეყვანილი რიცხვის ბოლო ციფრი და
დაბეჭდეთ ეკრანზე.

"""


def task9():
    Num = int(input("ნებისმიერი რიცხვი: "))
    lastDigit = Num % 10
    print("ბოლო ციფრი არის: ", lastDigit)


# task9()


"""
10) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ წელს და დაადგენთ არის თუ არა შეყვანილი რიცხვი ნაკიანი წელიწადი.
მაგ: 2012, 2016 წლები ნაკიანია. გაითვალისწინეთ, ნაკიანია წელიწადი, რომელიც უნაშთოდ იყოფა ოთხზე, გარდა იმ წლებისა
რომლებიც იყოფა 100-ზე მაგრამ არ იყოფა 400-ზე. მაგ. 2100, 2200, 2300 წლები არ არის ნაკიანი. 2000 წელი ნაკიანია

"""

def task10():
    year = int(input("შეიყვანეთ წელი"))
    if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        print("ეს წელი ნაკიანია")
    else: 
        print("ეს წელიარ არის ნაკიანი")

# task10()


"""
11) დაბეჭდეთ 5-ის ჯერადი რიცხვები 20-დან 125-ის ჩათვლით.

"""


def task11():
    results = []
    for i in range(20, 126):
        if i % 5 == 0:
            results.append(i)

    print(results)


# task11()


"""
12) დაბეჭდეთ 8-ის ჯერადი რიცხვები 200-დან 25-ის ჩათვლით კლებადობით.

"""


def task12():
    results = []
    for i in range(200, 25, -1):
        if i % 8 == 0:
            results.append(i)

    print(results)


# task12()


"""
13) შეიყვანთ 2 რიცხვი. ციკლის გამოყენებით დაბეჭდეთ შეყვანილი რიცხვების საერთო გამყოფები. მაგ. 15-ის
და 18-ის საერთო გამყოფებია: 1, 3

"""


def task13():
    num1 = int(input("პირველი რიცხვი: "))
    num2 = int(input("მეორე რიცხვი: "))

    common_divs = []
    for i in range(1, min(num1, num2) + 1):
        if num1 % i == 0 and num2 % i == 0:
            common_divs.append(i)
    print(common_divs)


# task13()


"""
14) შეიყვანეთ 10 რიცხვი კლავიატურიდან ციკლის გამოყენებით. დაითვალეთ შეყვანილი რიცხვების საშუალო არითმეტიკული.

"""


def task14():
    array = []
    for i in range(0, 10):
        array.append(i)
    print(sum(array) / len(array))


# task14()


"""
15) დაითვალეთ 1-დან 100-ის ჩათვლით ლუწი რიცხვების ჯამი და დაბეჭდეთ შედეგი

"""


def task15():
    array = []
    for i in range(1, 101):
        if i % 2 == 0:
            array.append(i)
    print(sum(array))


# task15()


"""
16) დაბეჭდეთ 1500-დან 2100-ის ჩათვლით რიცხვები რომლებიც არიან 7-ის და 5-ის ჯერადი ერთდროულად

"""

def task16():
    array = []
    for i in range(1500, 2101):
        if i % 7 == 0 and i % 5 == 0:
            array.append(i)
    print(array)


# task16()


"""
17) დაითვალეთ 1500-დან 2100-ის ჩათვლით რიცხვების ჯამი რომლებიც არიან 7-ის და 5-ის ჯერადი
ერთდროულად. დაბეჭდეთ მიღებული შედეგი

"""


def task17():
    array = []
    for i in range(1500, 2101):
        if i % 7 == 0 and i % 5 == 0:
            array.append(i)
    print(sum(array))


# task17()


"""
18) დაითვალეთ 1500-დან 2100-ის ჩათვლით რიცხვების ჯამი რომლებიც არიან 7-ის და 5-ის ჯერადი ერთდროულად.
როგორც კი რიცხვების ჯამი გადააჭარბებს 20 000-ს, შეწყვიტეთ ციკლი. დაბეჭდეთ მიღებული ჯამი ეკრანზე

"""


def task18():
    array = []
    for i in range(1500, 2101):
        if i % 7 == 0 and i % 5 == 0:
            array.append(i)
            sumArray = sum(array)
            if sumArray >= 20000:
                break
            print(sum(array))


# task18()


"""
19) დაითვალეთ 1500-დან 2100-ის ჩათვლით 5-ის ჯერადი რიცხვების რაოდენობა. დაბეჭდეთ შედეგი.

"""



def task19():
    count = 0
    for i in range(1500, 2101):
        if i % 5 == 0:
            count += 1
    print(count)


# task19()


"""
20) დაბეჭდეთ ეკრანზე 15-დან 150-მდე 5-ის ჯერადი რიცხვები გარდა 50-ის, 75, 80-ისა. გამოიყენეთ continue ოპერატორი.

"""

def task20():
    array = []
    for i in range(15, 151):
        if i % 5 == 0:
            if i == 50 or i == 75 or 80:
                continue
        array.append(i)

    print(array)


# task20()

"""
21) შეიყვანეთ 2 დადებითი მთელი რიცხვი. იპოვეთ ამ ორი რიცხვის უდიდესი საერთო გამყოფი.

"""


def task21():
    num1 = int(input("პირველი დადებითი, მთელი რიცხვი: "))
    num2 = int(input("მეორე დადებითი, მთელი რიცხვი: "))

    array = []
    for i in range(1, min(num1, num2) + 1):
        if num1 % i == 0 and num2 % i == 0:
            array.append(i)
    print(num1, "ის და ", num2, "ის უდიდესი საერთო გამყოფი არის", max(array))
    
    """
        def gcd(num1, num2):
        while num2 != 0:
            num1, num2 = num2, num1 % num2
        return num1

    print(f"უდიდესი საერთო გამყოფი {num1} და {num2} არის {gcd(num1, num2)}")

    """

# task21()


"""
22) შეიყვანეთ 2 დადებითი მთელი რიცხვი. იპოვეთ ამ ორი რიცხვის უმცირესი საერთო ჯერადი

"""


def task22():
    num1 = int(input("პირველი დადებითი, მთელი რიცხვი: "))
    num2 = int(input("მეორე დადებითი, მთელი რიცხვი: "))

    def gcd(num1, num2):
        while num2 != 0:
            num1, num2 = num2, num1 % num2
        return num1

    def lcm(num1, num2):
        return abs(num1 * num2) // gcd(num1, num2)

    print(f"უმცირესი საერთო ჯერადი {num1} და {num2} არის {lcm(num1, num2)}")


# task22()


"""
23) შეიყვანეთ 10 რიცხვი ციკლის გამოყენებით. იპოვეთ რიცხვებს შორის უდიდესი კენტი რიცხვი და დაბეჭდეთ.
თუ კენტი რიცხვი არ შეგხვდათ, გამოიტანეთ შესაბამისი შეტყობინება.

"""


def task23():
    array = []
    for i in range(1, 11):
        num = int(input(f"შეიყვანეთ ციფრი {i}: "))
        array.append(num)

    print("თქვენი შეყვანილი რიცხვები: ", array)

    largest_odd = None
    for num in array:
        if num % 2 != 0:
            if largest_odd is None or num > largest_odd:
                largest_odd = num

    if largest_odd is not None:
        print("ყველაზე დიდი კენტი რიცხვი არის:", largest_odd)
    else:
        print("კენტი რიცხვები მასივში არ არის!!!.")


# task23()

"""
24) შეიყვანეთ რიცხვი კლავიატურიდან. პროგრამამ უნდა დაბეჭდოს შეყვანილი რიცხვის ყველა გამყოფი. (მაგ. 18-ის
გამოყოფებია: 1, 2, 3, 6, 9, 18)

"""

def task24():
    number = int(input("შეიყვანეთ ნებისმიერი რიცხვი: "))
    array = []
    for i in range(1, number + 1):
        if number % i == 0:
            array.append(i)

    print(array)


# task24()

"""
25) შეიტანეთ რიცხვი. დაადგინეთ შეტანილი რიცხვი არის თუ არა მარტივი რიცხვი და გამოიტანეთ შესაბამისი შეტყობინება
(გაითვლისწინეთ: მარტივია რიცხვი, რომელსაც აქვს მხოლოდ ორი გამყოფი: ერთი და თავისი თავი)

"""


def task25():
    number = int(input("შეიყვანეთ ნებისმიერი რიცხვი: "))
    array = []
    for i in range(1, number + 1):
        if number % i == 0:
            array.append(i)

    if len(array) == 2:
        print(f"{number} - მარტივი რიცხვია")
    else:
        print(f"{number} - არ არის მარტივი რიცხვი")


# task25()

"""
26) დაბეჭდეთ 2-დან 1000-მდე ყველა მარტივი რიცხვი

"""

def task26():
    array = []
    for num in range(1, 1001):
        is_prime = True
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                is_prime = False
                break
        if is_prime:
            array.append(num)

    print("მარტივი რიცხვები 1 დან 1000 მდე: ", array)


# task26()


"""
27) დაბეჭდეთ 0-დან 100-მდე არსებული ფიბონაჩის რიცხვები (ფიბონაჩის რიცხვებია 1, 1, 2, 3, 5, 8, 13, ...)

"""


def task27():
    a, b = 1, 1
    fib_numbers = []

    while a <= 100:
        fib_numbers.append(a)
        a, b = b, a + b

    print("ფიბონაჩის რიცხვები 1 დან 100 მდე:", fib_numbers)


# task27()


"""
28) შეიყვანეთ ნებისმიერი რიცხი. იპოვეთ ამ რიცხვის ციფრთა რაოდენობა და დაბეჭდეთ.

"""

def task28():
    number = int(input("შეიყვანეთ ნებისმიერი რიცხვი: "))
    print(len(str(number)))
    count = 0
    while number > 0:
        number //= 10
        count += 1
    print("ციფრთა რაოდენობა არის ", count)


# task28()


"""
29) შეიყვანეთ ნებისმიერი რიცხი. იპოვეთ ამ რიცხვის ციფრთა ჯამი და დაბეჭდეთ.

"""

def task29():
    number = int(input("შეიყვანეთ ნებისმიერი რიცხვი: "))
    Sum = 0
    while number > 0:
        Sum += number % 10
        number //= 10
    print(f"ციფრთა ჯამი უდრის {Sum}")


# task29()


"""
30) შეიყვანეთ ნებისმიერი რიცხი. დაბეჭდეთ შეტანილი რიცხვი შებრუნებული სახით. (მითითება: მაგ.
თუ შეიტანთ 1254-ს, დაბეჭდოს 4521)

"""

def task30():
    number = int(input("შეიყვანეთ რიცხვი: "))
    reversed_number = 0
    while number > 0:
        digit = number % 10
        reversed_number = reversed_number * 10 + digit
        number //= 10
    print(digit, reversed_number)


# task30()
    

"""
31) შეიყვანეთ ნებისმიერი რიცხი. დაადგინეთ არის თუ არა შეტანილი რიცხვი პალინდრომი. (მითითება: პალინდრომია
რიცხვი, რომელიც მარჯვნიდან და მარცხნიდან ერთნაირად იკითხება). მაგ. 12521

"""

def task31():
    number = int(input("შეიყვანეთ რიცხვი: "))
    orig_number = number
    reversed_number = 0
    while number > 0:
        digit = number % 10
        reversed_number = reversed_number * 10 + digit
        number //= 10
    if orig_number == reversed_number:
        print(orig_number, "პალინდრომია")
    else:
        print(orig_number, 'არ არის პალინდრომი')


# task31()


"""
32) შეიყვანეთ რიცხვი. დათვალეთ ამ რიცხვის ფაქტორიალი და დაბეჭდეთ. მაგ. 5-ის ფაქტორიალი იგივია რაც 1 * 2 * 3 * 4 * 5

"""


def task32():
    number = int(input("შეიყვანეთ რიცხვი: "))
    result = 1
    for i in range(1, number + 1):
        result *= i
    print(f"{number}-ის ფაქტორიალი უდრის {result}-ს")


# task32()


"""
33) დაწერეთ პროგრამა რომელიც ეკრანზე გამოიტანს შემდეგ გამოსახულებას. მითითება: გამოიყენეთ 2 ჩადგმული for ციკლი

"""

def task33():
    for i in range(1, 4):
        print("*" * i)

    for j in range(4, 0, -1):
        print("*" * j)


task33()
