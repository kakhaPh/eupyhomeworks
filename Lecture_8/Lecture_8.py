import pprint
from scrapy.http import TextResponse
import pandas as pd
import requests
import numpy as np
import math
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.metrics import mean_squared_error

main_url = "https://home.ss.ge/ka/udzravi-qoneba/l/bina/iyideba?cityIdList=95&subdistrictIds=3&currencyId=1&priceType=1&priceFrom=1&page={}"
home_url = "https://home.ss.ge"

all_urls = []

flats = 2
page = 1

while len(all_urls) < flats:
    url = main_url.format(page)
    req = requests.get(url)
    resp = TextResponse(req.url, body=req.text, encoding='utf-8')

    urls = resp.css('div.sc-3c53144d-6 a::attr(href)').getall()
    
    if not urls:
        print('aloha shechema')

    all_urls.extend(urls)
    print(f"გვერდი {page} დაისკრაპა, {len(all_urls)} მონაცემი შეგროვდა.")
    page += 1


# print(len(all_urls))
# print(all_urls)


def flat_details(url):
    req = requests.get(url)
    resp = TextResponse(req.url, body=req.text, encoding='utf-8')
    
    price = resp.css('div.sc-fc425ced-6 span#price::text').get()
    area = resp.css('div.sc-479ccbe-1:nth-of-type(1) span.sc-6e54cb25-4::text').get()
    rooms = resp.css('div.sc-479ccbe-1:nth-of-type(2) span.sc-6e54cb25-4::text').get()
    bedrooms = resp.css('div.sc-479ccbe-1:nth-of-type(3) span.sc-6e54cb25-4::text').get()
    floor = resp.css('div.sc-479ccbe-1:nth-of-type(4) span.sc-6e54cb25-4::text').get()

    return {
        'url': url,
        'price': float(price.replace('$', '').replace(',', '').strip()),
        'area': float(area.replace('m²', '').strip()),
        'rooms': float(rooms),
        'bedrooms': float(bedrooms),
        'floor': float(floor.split('/')[0].strip()),
    }
all_listing_details = []

for url in all_urls[:flats]:
    full_url = home_url + url
    details = flat_details(full_url)
    all_listing_details.append(details)

df = pd.DataFrame(all_listing_details)

df.to_excel("Lecture_8/data.xlsx", index=False)

print("მონაცემები წარმატებით ჩაიწერა Lecture_8/data.xlsx")

# ბინის ფასის 50 იანი პროცენტილი
percent = 50
prices_array = df['price'].tolist()
per = int(len(prices_array) * percent / 100)
# sorted_prices = np.sort(prices_array)
sorted_prices = sorted(prices_array)
percentile = round(np.random.uniform(sorted_prices[per-1], sorted_prices[per]), 2)

# ჩემი გამოთვლილი პროცენტილი
print(f"ბინის ფასები: {prices_array}, პროცენტილი: {percentile}")

# numpy პროცენტილი
print(np.percentile(sorted_prices, percent))

# ფასების საშუალო არითმეტიკული
mean_price = sum(sorted_prices) / len(prices_array)
print(f"ფასების საშუალო არითმეტიკული: {mean_price}")

# სტანდარტული გადახრა
squared_diffs = [(price - mean_price) ** 2 for price in sorted_prices]
mean_squared_diffs = sum(squared_diffs) / len(squared_diffs)
std_dev = math.sqrt(mean_squared_diffs)
print(f"საშუალო სტანდარტული გადახრა: {std_dev}")


"""
რომელიმე ორი მახასიათებლისთვის ააგეთ წრფივი ან პოლინომიალური რეგრესიული მოდელი, იპოვეთ რამდენად ეფექტურია რეგრესიული მოდელი.
Price - Rooms
Price - Rooms, Area, Bedrooms, Floor

მოიძიეთ რეგრესიული მოდელის ალტერნატიული რომელიმე მეთოდი, ააგეთ შესაბამისი მოდელი, შეადარეთ რეგრესიულ მოდელს. 

"""

# მახასიათებლები ეფექტურობისთვის
# X = df['rooms'].values.reshape(-1, 1)

# მახასიათებლები ფასის დამოკიდებულებისთვის
X = df[['area', 'rooms', 'bedrooms', 'floor']].values
y = df['price'].values

# წრფივი რეგრესია
linear_model = LinearRegression()
linear_model.fit(X, y)

# კოეფიციენტები წრფივი რეგრესიისთვის
linear_coefficients = linear_model.coef_
linear_intercept = linear_model.intercept_

print("წრფივი რეგრესიის კოეფიციენტები (coeficients):", linear_coefficients)
print("წრფივი რეგრესიის ინტერსეპტი (Intercept):", linear_intercept)

# წრფივი რეგრესიის შეფასება
linear_r_squared = linear_model.score(X, y)
linear_mse = mean_squared_error(y, linear_model.predict(X))
print("წრფივი რეგრესიის R-squared:", linear_r_squared)
print("წრფივი რეგრესიის MSE:", linear_mse)

# Ridge რეგრესია
ridge_model = Ridge(alpha=1.0)
ridge_model.fit(X, y)

# Ridge რეგრესიის კოეფიციენტები
ridge_coefficients = ridge_model.coef_
ridge_intercept = ridge_model.intercept_

print("Ridge რეგრესიის კოეფიციენტები (Coefficients):", ridge_coefficients)
print("Ridge რეგრესიის ინტერსეპტი (Intercept):", ridge_intercept)

# Ridge რეგრესიის შეფასება
ridge_r_squared = ridge_model.score(X, y)
ridge_mse = mean_squared_error(y, ridge_model.predict(X))
print("Ridge რეგრესიის R-squared:", ridge_r_squared)
print("Ridge რეგრესიის MSE:", ridge_mse)

# მონაცემების (data points) რაოდენობა
num_analyses = len(X)
print("ანალიზის რაოდენობა (data points):", num_analyses)



"""

from scrapy.http import TextResponse
import requests
import pprint
import pandas as pd

url = requests.get("https://ss.ge/ka/udzravi-qoneba/l/bina/iyideba?MunicipalityId=95&CityIdList=95&StatusField.FieldId=34&StatusField.Type=SingleSelect&StatusField.StandardField=Status&PriceType=false&CurrencyId=1")
response = TextResponse(url.url, body=url.text, encoding='utf-8')
items = response.css("div.latest_desc div a::attr(href)").getall()

counter = 0

area = []
rooms = []
bedrooms = []
price = []
#
for item in items:
    # print(item)
    if item != "/ka/home/promopaidservices" and item != "/ka/home/agent":
        print(item)
        url_for_itmes = requests.get("https://ss.ge"+item)
        response_for_items = TextResponse(url_for_itmes.url, body=url_for_itmes.text, encoding='utf-8')
        item_info =  response_for_items.css("div.ParamsHdBlk text::text").extract()
        item_price = response_for_items.css("div.article_right_price::text").extract();
        if(len(item_info)!=0):
            counter += 1
            print(item_info)
            print(item_price[0].strip())
            area.append(item_info[0][:-2])
            rooms.append(item_info[1])
            bedrooms.append(item_info[2])
            price.append(item_price[0].strip())
            if counter==5:
                break
data = {
    "area":area,
    "rooms":rooms,
    "bedrooms":bedrooms,
    "price":price
}
df = pd.DataFrame(data)
print(df)
df.to_excel("ss.xlsx", index=False)

"""
