import numpy as np

def task3():
    N = 10
    Min = 300
    Max = 30000
    random_salary = np.random.randint(Min, Max, N)

    Percent_1 = 30
    Percent_2 = 70
    Percent_3 = 80

    per = int(len(random_salary) * Percent_1 / 100)
    print(f"{per} - არის ხელფასების {Percent_1}%")
    
    sorted_random_salary = np.sort(random_salary)
    
    percentile = round(np.random.uniform(sorted_random_salary[per-1], sorted_random_salary[per]), 2)

    print(sorted_random_salary, percentile, 'aloha')
    
    print(np.percentile(sorted_random_salary, Percent_1))

task3()
