import math
import random

"""
1.	შეიტანეთ ათწილადი რიცხვი, დაამრგვალეთ ათწილად ნაწილში მეათედის სიზუსტით 
(1 ციფრი ათწილად ნაწილში) და დაბეჭდეთ შედეგი. გამოიყენეთ round, ceil, floor, trunc 
ფუნქციები სათითაოდ და შეამოწმეთ შედეგი თითოეულის გამოყენებით. 

"""


def task1():
    floatNum = float(input("შეიყვანეთ ათწილადი რიცხვი: "))

    rounded_number = round(floatNum, 1)
    print("Round ფუნქცია:", rounded_number)

    ceiled_number = math.ceil(floatNum * 10) / 10
    print("ceil ფუნქცია:", ceiled_number)

    floored_number = math.floor(floatNum * 10) / 10
    print("floor ფუნქცია:", floored_number)

    truncated_number = math.trunc(floatNum * 10) / 10
    print("trunc ფუნქცია:", truncated_number)


# task1()

"""
2.	შეიტანეთ სამი რიცხვი, იპოვეთ მათ შორის მაქსიმუმი და დაბეჭდეთ შედეგი. გამოიყე¬ნეთ max ფუნქცია.

"""


def task2():
    Num1 = int(input("შეიყვანეთ პირველი რიცხვი: "))
    Num2 = int(input("შეიყვანეთ მეორე რიცხვი: "))
    Num3 = int(input("შეიყვანეთ მესამე რიცხვი: "))

    print(max(Num1, Num2, Num3))


# task2()

"""
გამოთვალეთ შემდეგი გამოსახულების შედეგი ფუნქციის გამოყენებით:
ა) √225625 ბ) √4225

"""

def task3():
    t = math.sqrt(225625)
    t = math.sqrt(4225)

# task3()

"""
4.	დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი დიაპაზონიდან 0-დან 1-ის ჩათვლით. დაამრგვალეთ რიცხვი (3 ციფრი ათწილად ნაწილში) და დაბეჭდეთ.

"""

def task4():
    t = random.random()
    print(round(t, 3))

# task4()


"""
5.	დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი 100-დან 120-მდე. დაამრგვალეთ რიცხვი (1 ციფრი ათწილად ნაწილში) და ისე გამოიტანეთ.

"""


def task5():
    random_float = random.uniform(100, 120)
    print("რანდომ რიცხვი 100 დან 120-მდე:", math.trunc(random_float * 10) / 10)


# task5()


"""
6.	დააგენერირეთ 10 შემთხვევითი მთელი რიცხვი და დაბეჭდეთ ეკრანზე. მითითება: გამოიყენეთ ციკლის ოპერატორი.

"""

def task6():
    for i in range(1, 11):
        random_float = random.uniform(0, 100)
        print(math.ceil(random_float * 10) / 10)


# task6()

"""
7.	შექმენით ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის (დააბრუნებს) მათ საშუალო არითმეტიკულს. გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის და დაბეჭდეთ შედეგი.

"""

def task7(num1, num2):
    print((num1 + num2) / 2)


# task7(12, 24)
# task7(18, 124)
# task7(152, 424)


"""
8.	დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის მათ საშუალო არითმეტიკულს და დაბეჭდავს შედეგს (გაითვალისწინეთ რომ დაბეჭდვა უნდა მოხდეს ფუნქციის შიგნით -  ფუნქცია არ აბრუნებს მნიშვნელობას). გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის.

"""

def task8(num1, num2):
    averageNum = (num1 + num2) / 2
    print(averageNum)


# task8(1410, 155)
# task8(25, 15)
# task8(160, 115)


"""
9.	შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) არგუმენტად გადაცემული რიცხვის კუბს. გამოიძახეთ ფუნქცია რამდენიმეჯერ და დაბეჭდეთ მიღებული 

"""


def task9(num):
    NumCube = num ** 3
    print(NumCube)


# task9(3)
# task9(4)
# task9(5)

"""
10.	შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) ორ რიცხვს შორის მინიმალურ მნიშვნელობას. გამოიძახეთ ფუნქცია და დაბეჭდეთ შედეგი. (პარამეტრად გადაეცით ნებისმიერი ორი რიცხვი).

"""

def task10(num1, num2):
    print(min(num1, num2))

# task10(123, 321)


"""
11.	დაწერეთ ფუნქცია, რომელიც შეამოწმებს პარამეტრად გადაცემული რიცხვი არის თუ არა კენტი. თუ კენტია, დააბრუნოს მნიშვნელობა True, თუ არადა - False. შეამოწმეთ რამდენიმე რიცხვისთვის და დაბეჭდეთ შედეგი.

"""

def task11(isOddorNot):
    if isOddorNot % 2 != 0:
        print(True)
    else:
        print(False)

# task11(1231231231312123124)

"""
12.	დაწერეთ ფუნქცია, რომელიც დაითვლის (დააბრუნებს) პარამეტრად გადაცემული რიცხვის ფაქტორიალს და დაბეჭდეთ შედეგი სხვადასხვა რიცხვებისთვის.

"""

def task12(num):
    factorial = 1
    for i in range(1, num + 1):
        factorial *= i
    print(factorial)

# task12(5)


"""
13.	დაწერეთ უპარამეტრო ფუნქცია რომელიც ეკრანზე ბეჭდავს შემდეგ ტექსტს: “Hello World”. (გაითვალისწინეთ რომ ფუნქცია არ აბრუნებს მნიშვნელობას).

"""


def task13():
    print("hello world")

# task13()

"""
14.	დაწერეთ ანონიმური ფუნქცია რომელიც დაითვლის რიცხვის კუბს.

"""

def task14():
    cube = lambda x: x ** 3
    print(cube(3))

# task14()

"""
15.	შექმენით სია numbs ნებისმიერ 5 რიცხვითი მნიშვნელობით. იპოვეთ ამ რიცხვების ჯამი, მინიმალური, მაქსიმალური და საშუალო არითმეტიკული. ასევე შეასრულეთ შემდეგი ოპერაციები:
•	სიას დაამატეთ ბოლო ელემენტად რიცხვი 102
•	სიის მესამე ელემენტად ჩასვით რიცხვი 205
•	წაშალეთ სიის მე-4 ელემენტი
•	დაალაგეთ სია ზრდადობის მიხედვით და დაბეჭდეთ.


"""

def task15():
    l = [5, 4, 1, 2, 3]
    SumList = sum(l)
    minList = min(l)
    maxList = max(l)
    averageList = sum(l) / len(l)
    l.append(102) # 102 დაემატა ბოლოში
    l.insert(2, 205) # 205 ჩასვა მესამე ელემენტად
    l.pop(3) # წაიშალა მეოთხე ელემენტი
    l.sort()
    print(l)
    print("სიაში მოცემული რიცხვების ჯამი: ", SumList)
    print("სიაში მოცემული რიცხვების მინიმალური რიცხვი: ", minList)
    print("სიაში მოცემული რიცხვების მაქსიმალური რიცხვი: ", maxList)
    print("სიაში მოცემული რიცხვების საშუალო არითმეტიკული: ", averageList)


# task15()


"""
16.	დაწერეთ პროგრამა, რომლის მეშვეობით შეიყვანთ (input-ით) 10 მონაცემს. წარმოადგინეთ და დაბეჭდეთ ისინი სიის ელემენტების სახის.

"""

def task16():
    l = []
    for i in range(1, 11):
        data = int(input(f"შეიყვანეთ მონაცემი {i}: "))
        l.append(data)
    print(l)

# task16()


"""
17.	შექმენით სია fruits, რომელის ელემენტებია: Watermelon, Banana, Apple. დაალაგეთ სიის ელემენტები ალფაბეტის უკუ-მიმართულებით და დაბეჭდეთ ისინი.

"""

def task17():
    fruits = ["Watermelon", "Banana", "Apple"]
    SortedFruits = sorted(fruits, key=str.lower)
    print(SortedFruits)

# task17()

"""
18.	დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა სია, დაითვლის სიის ელემენტების ნამრავლს და დააბრუნებს შედეგს. გამოიძახეთ ფუნქცია ნებისმიერი სიისთვის.

"""

def task18(list):
    result = 1
    for num in list:
        result *= num
    return result

list = [2, 4, 2, 1]
# result = task18(list)
# print(result)

"""
19.	დაწერეთ პროგრამა, რომელიც რიცხვითი მნიშვნელობების სიაში ამოშლის კენტ რიცხვებს. დაბეჭდეთ მიღებული სია.

"""

def task19():
    array = [1, 2, 3, 4, 5, 123, 23, 11, 12, 24]
    # even_array = [i for i in array if i % 2 == 0]
    for num in array[:]:
        if num % 2 != 0:
            array.remove(num)
    print(array)

# task19()

"""
20.	შექმენით 5 ელემენტიანი სია (რიცხვითი მნიშვნელობებით). თითოეული ელემენტი გაზარდეთ 10-ით და დაბეჭდეთ სია.

"""

def task20():
    list = [1, 2, 3, 4, 5]
    for nums in range(len(list)):
        list[nums] *= 10
    
    print(list)

# task20()

"""
21.	შექმენით 10 ელემენტიანი სია, რომლის ელემენტებია ნებისმიერი შემთხვევითი მთელი რიცხვები 25-დან 110-მდე. დაბეჭდეთ სია და იპოვეთ მინიმალური ელემენტი.

"""

def task21():
    list = []
    for _ in range(10):
        list.append(random.randint(25, 110))

    print(min(list))
    print(list)

# task21()

"""
22.	დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა 2 სია. ფუნქცია აბრუნებს მნიშვნელობა True-ს თუ სიებს აქვთ ერთი მაინც საერთო ელემენტი. წინააღმდეგ შემთხვევაში აბრუნებს False მნიშვნელობას.

"""

def task22(List_1=[], List_2=[]):
    for _ in range(10):
        List_1.append(random.randint(1, 5))
        List_2.append(random.randint(1, 5))

    print("List_1:", List_1)
    print("List_2:", List_2)

    if set(List_1) & set(List_2):
        print(True)
    else: 
        print(False)

# task22()

"""
23.	დაწერეთ ფუნქცია, რომელიც სიაში არსებულ კენტ რიცხვებს წაშლის. ფუნქცია აბრუნებს განახლებულ სიას.

"""

def task23():
    list = [1, 2, 3, 4, 5, 12333, 12, 13333, 1111]
    for num in list[:]:
        if num % 2 != 0:
            list.remove(num)

    print(list)

# task23()

"""
24.	შექმენით სია რიცხვითი ელემენტებით. shuffle ფუნქციის გამოყენებით (random მოდულიდან) მოახდინეთ სიის ელემენტების შემთხვევითად არევა და დაბეჭდეთ მიღებული სია. (მითითება: ფუნქცია იწერება შემდეგნაირად: random.shuffle(x) სადაც x სიის დასახელებაა)

"""


def task24():
    list = [1, 5, 6, 12, 23, 24, 7, 4, 13]
    random.shuffle(list)
    print(list)

# task24()

"""
25.	შექმენით სია რიცხვითი მნიშვნელობებით. რანდმულად ამოარჩიეთ სიის რომელიმე ელემენტი და დაბეჭდეთ. (მითითება: წინა სავარჯიშოს მსგავსად გამოიყენეთ random მოდულის choice ფუნქცია).

"""

def task25():
    list = [1, 5, 6, 12, 23, 24, 7, 4, 13]
    k = random.choice(list)
    print(k)

# task25()


"""
26.	დაწერეთ პროგრამა, რომელშიც შეიტანთ (input-ით) ნებისმიერ დიდ რიცხვს (მაგ. 342387410984). იპოვეთ რიცხვის ციფრთა ჯამი. (მითითება: თავდაპირველად გარდაქმენით რიცხვი სიად).

"""

def task26():
    num = int(input("შეიყვანეთ ნებისმიერი დიდი რიცხვი: "))
    j = str(num)

    list = []
    for _ in range(len(j)):
        k = num % 10
        num //= 10
        # list.append(k) # reversed
        list.insert(0, k)
    print(list)

# task26()


"""
27.	იპოვეთ სიაში [1, 5, 23, 5, 12, 2, 5, 1, 18, 5] ყველაზე ხშირად განმეორებადი რიცხვი. დაბეჭდეთ შედეგი. ასევე მიუთითეთ რამდენჯერ შეგხვდათ სიაში ყველაზე ხშირად განმეორებადი რიცხვი.

"""

def task27():
    list = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]
    # print(max(list, key=list.count))
    # print(list.count(max(list, key=list.count)))
    counts = {}

    for num in list:
        if num in counts:
            counts[num] += 1
        else: 
            counts[num] = 1
    
    max_count = max(counts.values())
    most_repeated_num = max(counts, key=counts.get)

    print(most_repeated_num)
    print(max_count)

# task27()


"""
28.	შექმენით სია extensions = ['txt', 'jpg', 'gif', 'html']. პროგრამის გაშვების შემდეგ მომხამრებელმა შეიყვანოს (input) ნებისმიერი ფაილის დასახელება. თუ ფაილის გაფართოება ემთხევა სიის რომელიმე ელემენტს, დაბეჭდოს ეკრანზე “Yes”, წინააღმდეგ შემთხვავაში დაბეჭდოს “No”.

"""

def task28():
    extensions = ['txt', 'jpg', 'gif', 'html']
    userInput = input("შეიყვანეთ გაფართოება: ")
    lowerUserInput = str.lower(userInput)
    if lowerUserInput in extensions:
        print("Yes")
    else:
        print("NO")

# task28()


"""
29.	სტრიქონი 'python php pascal javascript java c++' წარმოადგინეთ სიის სახით (სტრიქონის თითოეული სიტყვა სიის თითოეული ელემენტად). იპოვეთ სიის ყველაზე გრძელი ელემენტი (ანუ ყველაზე გრძელი სიტყვა).

"""

def task29():
    string = "python php pascal javascript java c++"
    k = string.split()
    print(k)

    longest_word = ""
    max_length = 0
    for i in k:
        if len(i) > max_length:
            longest_word = i
            max_length = len(i)
    
    print(longest_word, max_length)

# task29()

"""
30.	შეიტანეთ სიის 10 ელემენტი. იპოვეთ ამ რიცხვების საშუალო არითმეტიკული, მედიანა და მოდა. გაითვალისწინეთ, მედიანა წარმოადგენს შუა ელემენტს, როდესაც რიცხვები დალაგებულია ზრდადობით (ან კლებადობით); თუ შუაში ორი ელემენტია, მაშინ მედიანა არის ამ შუა ელემენტების საშუალო არითმეტიკული. მოდა, არის მიმდევრობაში რიცხვი, რომელიც ყველაზე ხშირად გვხვდება. შეიძლება მიმდევრობას არ ქონდეს მოდა (თუ ყველა ელემენტს ერთნაირი სიხშირე აქვს), ან ქონდეს ერთი ან რამდენიმე მოდა.

"""

def task30():
    list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # Average
    average = sum(list) / len(list)
    # Median
    srtList = sorted(list)
    if len(srtList) % 2 == 0:
        j = round(len(srtList) / 2)
        median = (list[j] + list[j+1]) / 2 
    else:
        j = round(len(srtList) / 2)
        median = list[j]

    counts = {}
    for num in list:
        if num in counts:
            counts[num] += 1
        else:
            counts[num] = 1
    mode = max(counts, key=counts.get)
    
    # Mode
    print(mode)
    # Median
    print(median)
    # Average
    print(average)

task30()