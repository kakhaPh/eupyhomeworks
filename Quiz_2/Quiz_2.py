import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

"""
დავუშვათ ორგანიზაციას ყავს 20 თანამშრომელი, თანამშრომლები ყოველ დღიურად, კვირაში 5 დღე გარკვეული დროის განმავლობაში ასრულებენ გარკვეულ სამუშაოს. 
ექსელში (ერთ ცხრილში) ჩაწერეთ შემდეგი მონაცემები:
1)	თანამშრომლების საიდენტიფიკაციო ნომერი წარმოადგენს განსხვავებულ შემთხვევ რიცხვებს [100; 999] შუალედიდან. (ჩაწერეთ ექსელის პირველ სვეტში).
2)	თანამშრომლის მიერ ერთი დღის განმავლობაში სამუშაოზე დახარჯული დრო, შემთხვევით ათწილადი რიცხვი [0, 10] შუალედში, მძიმის შემდეგ ორი ციფრით. დააგენერირეთ ხუთი (ერთ კვირის) ჩანაწერი თითეული თანამშრომლისთვის. (ჩაწერეთ ექსელის იმავე ცხრილში შემდეგ სვეტებში).
მითითება (ცხრილში საჭიროა ჩაიწეროს თანამშრომლის საიდენტიფიკაციო ნომერი + 5 დღის განმავლობაში შესრულებულ სამუშაოზე დახარჯული დრო, სულ 6 ველი).
3)	ააგეთ წერტილოვანი დიაგრამა ერთ კვირის განმავლობაში ერთი შემთხვევითად აღებული თანამშრომლის მიერ სამსახურში დახარჯულ დროსთან მიმართებაში, X ღერძის გასწვრივ კვირის დღეები, ხოლო Y ღერძის გასწვრივ თანამშრომლის მიერ კონკრეტულ დღეში დახარჯული დრო. 
4)	იპოვეთ ყველა თანამშრომლის მიერ სამუშაოზე კვირის განმავლობაში  დახარჯული დროის საშუალო, სტანდარტული გადახრა, მინიმალური, მაქსიმალური .

"""

N = 20

l = {
    'id': np.random.randint(100, 999, N),
    'day-1': np.round(np.random.uniform(1, 10, N), 2),
    'day-2': np.round(np.random.uniform(1, 10, N), 2),
    'day-3': np.round(np.random.uniform(1, 10, N), 2),
    'day-4': np.round(np.random.uniform(1, 10, N), 2),
    'day-5': np.round(np.random.uniform(1, 10, N), 2),
}

df = pd.DataFrame(l)

df.to_excel('data.xlsx')

statistics = pd.DataFrame({
    'Mean': df.mean(),
    'Standard Deviation': df.std(),
    'Minimum': df.min(),
    'Maximum': df.max()
})

print(statistics)


# data = df.melt(id_vars=['id'], var_name='Day', value_name='Number')

# # Plot
# plt.figure(figsize=(10, 6))
# plt.scatter(data['Day'], data['Number'], color='blue', alpha=0.5)
# plt.title('Dot Plot of Numbers Over Days')
# plt.xlabel('Days')
# plt.ylabel('Numbers')
# plt.grid(True)
# plt.xticks(rotation=45)
# plt.tight_layout()

# # Show plot
# plt.show()