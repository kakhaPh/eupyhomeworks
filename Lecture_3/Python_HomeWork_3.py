import numpy as np
import random

"""
1.	დაწერეთ პროგრამა რომლის მეშვებით შეიტანთ ნებისმიერ სტრიქონს (input ბრძანების გამოყენებით). დაბეჭდეთ შეყვანილი სტრიოქნის სიგრძე (სიმბოლოების რაოდენობა).

"""

def task1():
    string = input("შეიყვანეთ ნებისმიერი სტრიქონი: ")
    print(len(string))

# task1()


"""
2.	შეიყვანეთ ნებისმიერი ორი სტრიქონი. ახალ ცვლადში მოახდინეთ მათი შეერთება და დაბეჭდეთ შედეგი.

"""

def task2():
    string_1 = input("შეიყვანეთ ნებისმიერი სტრიქონი 1: ")
    string_2 = input("შეიყვანეთ ნებისმიერი სტრიქონი 2: ")
    string_3 = string_1 + " " + string_2
    print(string_3)

# task2()

"""
3.	შეიყვანეთ სტრიქონი. დაითვალეთ რამდენჯერ შეგხვდათ სიმბოლო “a” დაბეჭდეთ შედეგი. 

"""

def task3():
    string = input("შეიყვანეთ ნებისმიერი სტრიქონი: ")
    count_a = 0
    for char in string:
        if char == 'a':
            count_a += 1

    if count_a > 0:
        print(count_a)
    else:
        print("There is no a")

# task3()

"""
4.	შეიყვანეთ სამი სტრიქონი რომელიც წარმოადგენს სხვადასხვა ხილის დასახელებას (მაგ. Banana, Watermelon, Apple). დაბეჭდეთ ისინი ალფაბეტის ზრდადობის  მიხედვით.

"""

def task4():
    string_1 = "Banana"
    string_2 = "Apple"
    string_3 = "Watermelon"

    strings = [string_1, string_2, string_3]

    sorted_string = sorted(strings)
    print(sorted_string)

    for i in sorted_string:
        print(i)

# task4()


"""
5.	text ცვლადს მიანიჭეთ სტრიქონი “სწავლის ძირი მწარე არის, კენწეროში გატკბილდების”. სტრიქონიდან წაიკითხეთ პირველი 20 სიმბოლო და დაბეჭდეთ შედეგი. დაითვალეთ რამდენჯერ არის ნახსენები სიმბოლო ‘ს’.

"""

def task5():
    text = "სწავლის ძირი მწარე არის, კენწეროში გატკბილდების"
    print(text[0:20])
    count_s = 0
    for char in text:
        if char == 'ს':
            count_s += 1

    j = text.count('ს')
    print(count_s, j)

# task5()


"""
6.	text ცვლადს მიანიჭეთ სტრიქონი “Hello, this is example of string. Please, write this text and do some exercises.”. თუ სტრიქონში გვხვდება ტექსტი “is“, შეცვლეთ ის “were”-ით და დაბეჭდეთ შედეგი. 

"""

def task6():
    text = "Hello, this is example of string. Please, write this text and do some exercises."
    if "is" in text:
        text = text.replace("is", "were")

    print(text)

# task6()

"""
7.	text ცვლადს მიანიჭეთ სტრიქონი “Hello, this is example of string. Please, write this text and do some exercises.” დაითვალეთ სტრიქონში სიტყვების რაოდენობა. 

"""

def task7():
    text = "Hello, this is example of string. Please, write this text and do some exercises."
    k = text.split()
    print(len(k))

# task7()

"""
8.	text ცვლადს მიანიჭეთ შემდეგი ტექსტი: “Have a nice day”. დაბეჭდეთ ტექსტის თითოეული სიმბოლო ეკრანზე უკუ მიმართულებით ცალ-ცალკე ხაზზე (ანუ პირველად დაბეჭდავ ბოლო სიმბოლოს, შემდეგ ბოლოს წინა სიმბოლოს, და ა.შ). 

"""

def task8():
    text = "Have a nice day"
    # for i in reversed(text):
    #     print(i)
    for i in range(len(text) - 1, -1 , -1):
        print(text[i])

# task8()

"""
9.	ტექსტურ ცვლადში ჩაწერეთ თქვენი ემაილი. სტრიქონის ფუნქციების გამოყენებით, იპოვეთ რომელ პოზიციაზეა განთავსებული სიმბოლო @. დაბეჭდეთ შედეგი.

"""

def task9():
    email = "kakha.putkaradze@eu.edu.ge"
    t = email.split("@")
    t2 = email.index("@")
    print(t2)
    print(len(t[0]))

# task9()

"""
10.	დაწერეთ პროგრამა, სადაც user შეიყვანს თავის სახელს და გვარს და პროგრამა დაუბეჭდავს შესაბამის იმეილის დასახელებას: მაგ: adam.giorgadze@edu.ge

"""

def task10():
    userName = input("შეიყვანეთ თქვენი სახელი გვარი: ")
    k = userName.replace(" ", ".")
    emailAddress = "@eu.edu.ge"
    userEmail = k + emailAddress
    print(userEmail)

# task10()


"""
11.	დაყავით რაიმე სტრიქონი N ტოლ ნაწილად. მაგალითად 3-3 სიმბოლოდ, ან 5-5 სიმბოლოდ და ა.შ. 

"""

def task11():
    text = "Hello, this is example of string. Please, write this text and do some exercises."
    divided_text = []
    for i in range(0, len(text), 5):
        divided_text.append(text[i:i+5])
    print(divided_text)

    for group in divided_text:
        print(group)

# task11()


"""
12.	მოახდინეთ ორი ორი ვექტორი შეკრების მოდელირება. მოიყვანეთ ორი მაგალითი (შემთხვევითი და სტატიკური რიცხვებისთვის)

"""

def task12(): 
    # vector1 = [1, 2, 3]
    # vector2 = [2, 3, 4]
    vector1 = [random.randint(1, 10) for _ in range(3)]
    vector2 = [random.randint(1, 10) for _ in range(3)]

    sum_vectors = [vector1[i] + vector2[i] for i in range(len(vector2))] 

    print(vector1) 
    print(vector2) 
    print(sum_vectors) 
# task12()


"""
13.	მოახდინეთ ორი მატრიცის შეკრების მოდელირება. მოიყვანეთ ორი მაგალითი (შემთხვევითი და სტატიკური რიცხვებისთვის)


"""

def task13():
    matrix1 = [[random.randint(1,10) for _ in range(3)] for _ in range(3)]
    matrix2 = [[random.randint(1,10) for _ in range(3)] for _ in range(3)]

    result = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

    for i in range(len(matrix1)):
        for j in range(len(matrix2)):
            result[i][j] = matrix1[i][j] + matrix2[i][j]

    for row in result:
        print(row)

# task13()

"""
14.	მოახდინეთ ორი ვექტორის გამრავლების მოდელირება. მოიყვანეთ ორი მაგალითი (შემთხვევითი და სტატიკური რიცხვებისთვის)

"""

def task14():
    # vector1 = [1, 2, 3]
    # vector2 = [1, 2, 3]
    
    vector1 = [random.randint(1, 10) for _ in range(3)]
    vector2 = [random.randint(1, 10) for _ in range(3)]


    dot_product = sum(x * y for x, y in zip(vector1, vector2))
    element_wise_product = [x * y for x, y in zip(vector1, vector2)]

    print(vector1)
    print(vector2)
    print(dot_product)
    print(element_wise_product)

# task14()

"""
15.	შეავსეთ 3x3 მატრიცა შემთხვევითი რიცხვებით [0, 10] შუალედიდან. გამოიყენეთ numpy ბიბლიოთეკა.

"""

def task15():
    # matrix = [
    #     [0, 0, 0],
    #     [0, 0, 0],
    #     [0, 0, 0]
    # ]

    # for i in range(len(matrix)):
    #     for j in range(len(matrix)):
    #         matrix[i][j] += np.random(1,10)

    matrix = np.random.randint(1, 11, size=(3,3))
    print(matrix)

# task15()


"""
16.	შეავსეთ 10x10 მატრიცა შემთხვევითი რიცხვებით [0, 10] შუალედიდან. წაშალეთ კლავიატურიდან შეტანილი რიცხვი. გამოიყენეთ numpy ბიბლიოთეკა.

"""


def task16():
    matrix = np.random.randint(0, 11, size=(2, 2))
    print(matrix)

    k = int(input("შეიყვანეთ 0, 10 მდე ნებისმიერი ციფრი: "))
    
    
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if matrix[i, j] == k:
                matrix[i, j] = 0

    print(matrix)


# task16()


"""
17.	შეავსეთ 10x10 მატრიცა შემთხვევითი რიცხვებით [0, 10] შუალედიდან. წაშალეთ კლავიატურიდან შეტანილი სვეტი. გამოიყენეთ numpy ბიბლიოთეკა.

"""

def task17():
    matrix = np.random.randint(0, 11, size=(2, 2))
    print(matrix)

    k = int(input("შეიყვანეთ 0, 10 მდე ნებისმიერი ციფრი: "))
    
    if 1 <= k <= matrix.shape[1]:
        matrix = np.delete(matrix, k-1, axis=1)
        print(matrix)


# task17()


"""
18.	შეავსეთ 10x10 მატრიცა შემთხვევითი რიცხვებით [0, 10] შუალედიდან. შეცვალეთ ყველა 0-ის მნიშვნელობა 1-ით. გამოიყენეთ numpy ბიბლიოთეკა.

"""


def task18():
    matrix = np.random.randint(0, 10, size=(10, 10))
    print(matrix)
    matrix[matrix == 0] = 1

    print(" ")
    print(matrix)

# task18()



"""
19. ააგეთ ჭადრაკის დაფა. გამოიყენეთ numpy ბიბლიოთეკა.

"""

def task19():
    chessboard = np.indices((8,8)).sum(axis=0) % 2

    print(chessboard)
task19()
