import pandas as pd
import random
import string

"""
უნივერსიტეტში 500 სტუდენტი გადის სავალდებულო 3 საგანს (English, Computer Skills, Academic Writing) 

მონაცემების გენერირება
    1. სტუდენტებისთვის დააგენერირეთ 500 შემთხვევითი 11 ინგლისური ანბანის ასოებისგან და ციფრებისგან შემდგარი უნიკალური იდენტიფიკატორი (სტრიქონი) და ჩაწერეთ data.xlsx-ის ფაილის პირველ სვეტში.
    2. თითოეული საგნისთვის დააგენერირეთ შემთხვევით მთელი რიცხვები 0-დან 100-მდე და ჩაწერეთ ექსელის შემდეგ სვეტებში. 

მონაცემების წაკითხვა და ანალიზი
    1. წაიკითხეთ data.xlsx ფაილი, გამოთვალეთ თითოეული საგნისთვის საშუალო არითმეტიკული.
    2. დაბეჭდეთ თითეულ საგანში რამდენმა სტუდენტმა აიღო 51 ქულაზე მეტი ან ტოლი.  

""" 

def task():
    id = []
    students_above_51 = 0

    for _ in range(500):
        identifier = ''.join(random.choices(string.ascii_letters + string.digits, k=11))
        english_score = random.randint(0, 100)
        computer_skills_score = random.randint(0, 100)
        academic_writing_score = random.randint(0, 100)
        average_score = (english_score + computer_skills_score + academic_writing_score) / 3
        id.append({
            'Id': identifier,
            'English': english_score,
            'Computer Skills': computer_skills_score,
            'Academic Writing': academic_writing_score,
            'Average': average_score
        })

        if english_score >= 51 or computer_skills_score >= 51 or academic_writing_score >= 51:
            students_above_51 += 1

    data = pd.DataFrame(id)
    data.to_excel('data.xlsx', index=False)
    print(f"51 ზე მეტი აიღო: {students_above_51} სტუდენტმა")

task()