import random
"""
განსაზღვრეთ A სია, ჩაწერეთ სიაში 10 სახელი ინგლისურ ენაზე (მაგ: „Anna”, “Zura” და ა.შ.) კლავიატურიდან ციკლის
საშუალებით. გადაწერეთ B სიაში A-ს ის სახელები, რომლებიც შეიცავს სიმბოლო “b”-ს, ხოლო C სიაში A-ის სახელები,
რომლებიც შიცავს სიმბოლო “n”-ს. დაითვალეთ B და C სიაში არსებული ელემენტების რაოდენობა, დაადგინეთ რომელია
მეტი. დაბეჭდეთ სამივე სია. 

"""

def task_1():
    names_a = []
    names_b = []
    names_c = []
    for i in range(10):
        name = input(f"ჩაწერეთ სახელი {i}): ")
        names_a.append(name)
        if 'b' in name.lower():
            names_b.append(name)

        if 'n' in name.lower():
            names_c.append(name)
    print("ჩაწერილი სახელები", names_a)
    print("სახელები რომლებიც შეიცავს ასო  'b' - ს:", names_b)
    print("სახელები რომლებიც შეიცავს ასო 'n' - ს ", names_c)

    print(f"'b'ასოს შეიცავს {len(names_b)} სახელი, ხოლო 'n'-ასოს შეიცავს {len(names_c)} სახელი")


task_1()

"""
დაწერეთ პროგრამა, რომელიც შექმნის შემდეგი სახის ლექსიკონს (key არის 1-დან 100-მდე შემთხვევი განსხვავებული
რიცხვები, ხოლო value- მათი ციფრების ჯამი). ლექსიკონი შედგება 10 ელემენტისგან. დაბეჭდეთ ყველაზე დიდი და მცირე
მნიშვნელობის მქონდე ელემენტი, თუ ლექსიკონის ყველა ელემენტს ერთ სტრიქონში დავალაგებთ, გამოიტანეთ რამდენი
ნული იქნება მწკრივში. (4 ქულა)

"""


def task_2():
    lexicon = {}
    while len(lexicon) < 10:
        key = random.randint(1, 100)
        value = sum(int(digit) for digit in str(key))
        lexicon[key] = value

    print("დააგენერერირე ლექსიკონი:")
    for key, value in lexicon.items():
        print(f"{key}: {value}")

    max_key = max(lexicon, key=lexicon.get)
    min_key = min(lexicon, key=lexicon.get)
    print(f"მაქსიმალური ელემენტი: {max_key}: {lexicon[max_key]}")
    print(f"მინიმალური: {min_key}: {lexicon[min_key]}")


task_2()

# Kakha Phutakraze
