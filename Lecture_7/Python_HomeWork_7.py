import numpy as np
import pandas as pd
from scipy import stats
import statistics as s
import math
import random
import matplotlib.pyplot as plt


"""
მოდა

"""

def task1():
    speed = [99,86,87,88,111,86,103,87,94,78,77,85,86,87]
    x = stats.mode(speed)
    x1 = s.multimode(speed)

    print(x)
    print(x1)

# task1()


"""
პროცენტილის გამოთვლა

"""

def task2():
    N = 10

    Min = 100
    Max = 1000

    par = 30

    sal = np.random.randint(Min, Max, N)

    print(sal)

    lessMean = np.sum(sal<sal.mean())

    # საშუალო არითმეტიკული
    print(sal.mean())
    print(lessMean)

    # countPar = math.floor(N * par / 100)
    countPar = int(len(sal) * par / 100)

    print(f"N ის პარ % არის {countPar} ")

    sortSal = np.sort(sal)
    print(sortSal)

    print(sortSal[countPar-1], sortSal[countPar])

    percentile = round(np.random.uniform(sortSal[countPar-1], sortSal[countPar]), 2)
    print(percentile)
    # print(np.percentile(sal, 50))

# task2()


"""
კომპანია "X"-ში 500 დასაქმებულია

მონაცემების გენერირება

დასაქმებულებისთვის numpy-ის საშუალებით დააგენერირეთ შემთხვევით ხელფასები 300 ლარიდან 30 000 ლარის ჩათვლით.
მონაცემებისთვის გამოთვალეთ 30-იანი, 70-იანი, 80-იანი დონის პროცენტილი. 


"""

def task3():
    N = 500
    Min = 300
    Max = 30000
    random_salary = np.random.randint(Min, Max, N)

    Percent_1 = 30
    Percent_2 = 70
    Percent_3 = 80

    per = int(len(random_salary) * Percent_1 / 100)
    sorted_random_salary = np.sort(random_salary)
    
    percentile = round(np.random.uniform(sorted_random_salary[per-1], sorted_random_salary[per]), 2)

    print(sorted_random_salary, percentile)
    print(np.percentile(sorted_random_salary, Percent_1))

task3()


"""
კომპანია "Y"-ში 500 დასაქმებულია
სახელფასო ვექტორი შემდეგია [300, 450, 700, 850, 1700, 1950, 2000, 3500, 12000, 30000]
მონაცემების გენერირება
სახელფასო ვექტორის შესაბამისად დააგენერირეთ შემთხვევით ხელფასები 500-ვე დასაქმებულისთვის და ჩაწერეთ salary.xlsx ფაილში.
ააგეთ სახელფასო რაოდენობრივი გრაფიკი.

"""

def task4():
    Id = []
    y = 10
    salary_vector = [300, 450, 700, 850, 1700, 1950, 2000, 3500, 12000, 30000]
    for _ in range(y):
        k = random.choice(salary_vector)
        Id.append(({
            'salary': k,
        }))

    df = pd.DataFrame(Id)
    df.to_excel('salary.xlsx', index=False)

    plt.bar(range(min(df['salary'])), max(df['salary']))
    plt.grid(True)
    plt.show()

task4()

# N = 10

# random_salary = np.random.randint(300, 30000, N)

# # print(random_salary)

# thirty_prcent = int(len(random_salary) * 0.3)

# # print(thirty_prcent)
# myArray = [1, 2, 3, 4, 5]
# selected_elements = np.random.choice(myArray, size=thirty_prcent, replace=False)

# print(selected_elements)

