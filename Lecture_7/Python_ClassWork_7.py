import random
import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

# speed = [99, 100, 87, 95, 96, 98, 99, 88]
#
# x = stats.mode(speed)
#
# print(x)

# რიცხვი რომელზე ნაკლები ან ტოლია  მონაცემების 70%

def task():
    n = 10
    less_than_average = 0

    par = 30

    salary = np.random.randint(100, 1000, size=n)

    average = salary.mean()
    sorted_salary = sorted(salary)

    for i in salary:
        if i < average:
            less_than_average += 1

    percentage = round(less_than_average/n*100, 2)

    print(salary)
    print(sorted_salary)

    print(average)
    print(less_than_average)
    print(f"{percentage}%")

# task()


def task2():
    Id = []
    y = 10
    salary_vector = [300, 450, 700, 850, 1700, 1950, 2000, 3500, 12000, 30000]
    for _ in range(y):
        k = random.choice(salary_vector)
        Id.append(({
            'salary': k,
        }))

    df = pd.DataFrame(Id)
    df.to_excel('salary.xlsx', index=False)

    plt.bar(range(min(df['salary'])), max(df['salary']))
    plt.grid(True)
    plt.show()

task2()

# id = []
# students_above_51 = 0
#
# for _ in range(500):
#     identifier = ''.join(random.choices(string.ascii_letters + string.digits, k=11))
#     english_score = random.randint(0, 100)
#     computer_skills_score = random.randint(0, 100)
#     academic_writing_score = random.randint(0, 100)
#     average_score = (english_score + computer_skills_score + academic_writing_score) / 3
#     id.append({
#         'Id': identifier,
#         'English': english_score,
#         'Computer Skills': computer_skills_score,
#         'Academic Writing': academic_writing_score,
#         'Average': average_score
#     })
#
#     if english_score >= 51 or computer_skills_score >= 51 or academic_writing_score >= 51:
#         students_above_51 += 1
#
# data = pd.DataFrame(id)
# data.to_excel('data.xlsx', index=False)

# def calculate_mode(numbers_list: list):
#     frequency = {}
#
#     mode = []
#
#     for num in numbers_list:
#         frequency[num] = frequency.get(num, 0) + 1
#
#     most_occurrences = max(frequency.values())
#
#     if most_occurrences > 1:
#         for key, value in frequency.items():
#             if value == most_occurrences:
#                 mode.append(key)
#     else:
#         return ('No Mode Found!')
#
#     if len(mode) == 1:
#         return (mode[0])
#     return mode
#
#
# print(calculate_mode([10, 13, 5, 4, 17]))
# print(calculate_mode([10, 13, 5, 4, 17, 17]))
# print(calculate_mode([10, 13, 5, 4, 4, 17, 17, 16, 16]))