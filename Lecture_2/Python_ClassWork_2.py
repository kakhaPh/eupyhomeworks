# List()
l = [3, 8, 9, 10, "EU", "EU123"]

print(l)

l.append(76)

print(l)

l.insert(3, 78)
print(l)

# Tuples()

t = (2, 4, 4, 4, 5, 8.9, "EU")

print(t)

print(t[2])

t = t + (3, 5)

print(t)

t_l = list(t)
t_l.append("New ...")

n_t = tuple(t_l)

print(n_t)

# Set()
s = {3, 3, 4, 5, 1, 4, 5, 1, 2, "EuSTud", "A", "B", "C"}
print(s)

K = [4, 5, 6, 1, 4]
s1 = set(K)

print(s1)




d = {'name': "KP", "GPA": 4, "University": "European University"}

print(d['name'])

x = lambda a, b: a * b
print(x(5, 6))
